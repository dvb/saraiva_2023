DOPAMINERGIC NEURONS analysis (made by Axel Chemla- LCSB CEN group): 
	- Calcium imaging
	- Cell Rox
	- TMRE

MIDBRAIN ORGANOIDS analysis (made by Claudia Saraiva - LCSB DVB group):
	- TH/TUJ1/MAP2: S:\HCS_Platform\Scripts_Repository\ClaudiaSaraiva\Yokogawa\Manuscript_Miro1\TH-MAP2-TUJ1
	- TUNEL/TH/MAP2: S:\HCS_Platform\Scripts_Repository\ClaudiaSaraiva\Yokogawa\Manuscript_Miro1\TUNEL-TH-MAP2

Metabolomics MO data: adapted from Zagare et al., 2023 ()


R Scripts (maybe we upload to Gitlab closer to the submission date, in case any changes are needed):

scRNA seq data analysis: 

function [Summary] = f_imageAnalysis(ch1all, ch2all, ch3all, WellThis, FieldThis, MesFile, PreviewPath)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% vol(ch1all, 0,1000)
    Summary = table();

    %% segment nuclei
    ch1 = max(ch1all, [], 3); % imtool(ch1, [])
    NucLP = imfilter(ch1, fspecial('gaussian', 55, 5), 'symmetric'); % imtool(NucLP,[])
    %%figure; surf(fspecial('gaussian', 55, 11))
    NucMask = NucLP > 140; % imtool(NucMask,[])
    NucMask = bwareaopen(NucMask, 200);
    [NucLM, NucCount] = bwlabeln(NucMask); %imtool(NucLM,[])
    NucObjects = regionprops('table', NucLM, ch1, {'MeanIntensity','Area','MajorAxisLength','MinorAxisLength','Perimeter'});
    NucSummary = table();
    NucSummary = varfun(@(x) mean(x), NucObjects);
    NucSummary.SumArea = sum(NucMask(:));
    NucSummary.Count = NucCount;
    NucSummary.Properties.VariableNames = strcat('Nuc_', NucSummary.Properties.VariableNames);


    %% Segment mitochondria
    ch2 = max(ch2all, [], 3); % imtool(ch2, []) % MTgreen
    ch3 = max(ch3all, [], 3); % imtool(ch3, []) % TMRE
    % ch2 and ch3 should have similar intensity
    chMito = imlincomb(0.9, ch2, 0.1, ch3); %imtool(chMito, [])

    MitoDoG = imfilter(chMito, fspecial('gaussian', 33, 1) - fspecial('gaussian', 33, 3), 'symmetric'); % imtool(MitoDoG,[])
    MitoMaskMGall = MitoDoG > 10; % imtool(MitoMask,[])
    MitoMaskMGall = bwareaopen(MitoMaskMGall, 5);
    MitoMaskMGbright = MitoDoG > 200; % imtool(MitoMaskMGbright,[])
    MitoMaskMGbright = bwareaopen(MitoMaskMGbright, 5);
    [MitoLMMGall, MitoCountMGall] = bwlabeln(MitoMaskMGall); %imtool(MitoLMMGall,[])
    [MitoLMMGbright, MitoCountMGbright] = bwlabeln(MitoMaskMGbright); %imtool(MitoLMMGbright,[])
    MitoObjectsAll = regionprops('table', MitoLMMGall, ch3, {'MeanIntensity','Area','MajorAxisLength','MinorAxisLength','Perimeter'});
    MitoObjectsAll.Properties.VariableNames = strcat('All_', MitoObjectsAll.Properties.VariableNames);
    MitoObjectsBright = regionprops('table', MitoLMMGbright, ch3, {'MeanIntensity','Area','MajorAxisLength','MinorAxisLength','Perimeter'});
    MitoObjectsBright.Properties.VariableNames = strcat('Bright_', MitoObjectsBright.Properties.VariableNames);
%     MitoSummary = table();
%     MitoSummary = varfun(@(x) mean(x), MitoObjects);
%     MitoSummary.SumArea = sum(MitoMask(:));
%     MitoSummary.Count = MitoCount;
%     MitoSummary.Properties.VariableNames = strcat('Mito_', MitoSummary.Properties.VariableNames);
    MitoSummaryAll = table();
    MitoSummaryAll = varfun(@(x) mean(x), MitoObjectsAll);
    MitoSummaryAll.SumArea = sum(MitoMaskMGall(:));
    MitoSummaryAll.Count = MitoCountMGall;
    MitoSummaryAll.Properties.VariableNames = strcat('MitoAll_', MitoSummaryAll.Properties.VariableNames);
    
    MitoSummaryBright = table();
    MitoSummaryBright = varfun(@(x) mean(x), MitoObjectsBright);
    MitoSummaryBright.SumArea = sum(MitoMaskMGbright(:));
    MitoSummaryBright.Count = MitoCountMGbright;
    MitoSummaryBright.Properties.VariableNames = strcat('MitoBright_', MitoSummaryBright.Properties.VariableNames);


    
    MetaColumns = table();
    MetaColumns.Well = WellThis;
    MetaColumns.Field = FieldThis;
    %MetaColumns.Timepoint = t;
    MetaColumns.Mes = {MesFile};

    %% Collect outputs
    Summary = [MetaColumns, MitoSummaryAll, MitoSummaryBright, NucSummary];

    %% Previews

    imSize = size(MitoMaskMGall);
    [BarMask, BarCenter] = f_barMask(20, 0.10758027143330025, imSize, imSize(1)-50, 50, 20);
    %it(BarMask)

    NucPreview = f_imoverlayIris(imadjust(ch1, [0 0.01], [0 1]), imdilate(bwperim(NucMask),strel('disk', 1)), [0 0 1]);
    NucPreview = f_imoverlayIris(NucPreview, BarMask, [1 1 1]);
    %imtool(NucPreview)


    MitoPreviewMTgreenAll = f_imoverlayIris(imadjust(ch2, [0 0.03], [0 1]), bwperim(MitoMaskMGall), [1 0 0]);
    MitoPreviewMTgreenAll = f_imoverlayIris(MitoPreviewMTgreenAll, BarMask, [1 1 1]);
    %imtool(MitoPreviewMTgreenAll)

    %MitoPreviewTMREAll = f_imoverlayIris(imadjust(ch3, [0 0.03], [0 1]), bwperim(MitoMask), [1 0 0]);
    MitoPreviewTMREAll = f_imoverlayIris(imadjust(ch3, [0 0.2], [0 1]), bwperim(MitoMaskMGall), [1 0 0]);
    MitoPreviewTMREAll = f_imoverlayIris(MitoPreviewTMREAll, BarMask, [1 1 1]);
    %imtool(MitoPreviewTMREAll)
    

    MitoPreviewMTgreenBright = f_imoverlayIris(imadjust(ch2, [0 0.03], [0 1]), bwperim(MitoMaskMGbright), [1 0 0]);
    MitoPreviewMTgreenBright = f_imoverlayIris(MitoPreviewMTgreenBright, BarMask, [1 1 1]);
    %imtool(MitoPreviewMTgreenBright)

    %MitoPreviewTMREBright = f_imoverlayIris(imadjust(ch3, [0 0.03], [0 1]), bwperim(MitoMask), [1 0 0]);
    MitoPreviewTMREBright = f_imoverlayIris(imadjust(ch3, [0 0.2], [0 1]), bwperim(MitoMaskMGbright), [1 0 0]);
    MitoPreviewTMREBright = f_imoverlayIris(MitoPreviewTMREBright, BarMask, [1 1 1]);
    %imtool(MitoPreviewTMREBright)
    
    
    NucPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_Nuc.png'];
    MitoMTgreenPreviewPathAll = [PreviewPath, filesep, WellThis, '_', FieldThis, '_MitoMTgreenAll.png']
    MitoMTgreenPreviewPathBright = [PreviewPath, filesep, WellThis, '_', FieldThis, '_MitoMTgreenBright.png']
    MitoTMREPreviewPathAll = [PreviewPath, filesep, WellThis, '_', FieldThis, '_MitoTMREAll.png']
    MitoTMREPreviewPathBright = [PreviewPath, filesep, WellThis, '_', FieldThis, '_MitoTMREBright.png']

    imwrite(NucPreview, NucPreviewPath)
    imwrite(MitoPreviewMTgreenAll, MitoMTgreenPreviewPathAll)
    imwrite(MitoPreviewMTgreenBright, MitoMTgreenPreviewPathBright)
    imwrite(MitoPreviewTMREAll, MitoTMREPreviewPathAll)
    imwrite(MitoPreviewTMREBright, MitoTMREPreviewPathBright)


end


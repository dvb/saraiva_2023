function [] = f_plotAllKinetics(OutPath)
%Plot all kinetics

    csvFolder = OutPath; %'/mnt/IrisHCS/Data/AxelChemla/CaNeuronsReinhard/AC_20220225_Fluo4Direct_Fluo4BaptaNeud32_plate96_out'
    %ls([csvFolder, '*.csv'])
    %dir([csvFolder, '*.csv'])
    %[a,b] = system('ls /mnt/IrisHCS/Data/AxelChemla/CaNeuronsReinhard/AC_20220225_Fluo4Direct_Fluo4BaptaNeud32_plate96_out/*.csv')
    [a,b] = system(['ls ', csvFolder, '/*.csv'])
    files = [strsplit(b, '\n')]'
    files = files(1:end-1);

    Wells = regexp(files, '.*/(.*)_', 'tokens');
    Wells = cellfun(@(x) x{:}{:}, Wells, 'UniformOutput', false);
    Wells = unique(Wells);
    KineticsFigure = figure
    progress = 0;
    for i = 1:numel(Wells)
        progress = progress + 1;
        filesThisWell = strfind(files, Wells{i});
        filesThisWell = files(cellfun(@(x) ~isempty(x), filesThisWell));
        for j = 1:numel(filesThisWell)
            filesThisWell{j}
            vecThis = table2array(readtable(filesThisWell{j}));
            vecThisNorm2to = vecThis / vecThis(1);
            subplot(numel(Wells),1, i)
            plot(vecThisNorm2to)
            hold on
        end
        title(Wells{i})

    end
    saveas(KineticsFigure, [OutPath, filesep, 'kinetics.fig'])
end
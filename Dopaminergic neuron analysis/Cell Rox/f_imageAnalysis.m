function [Summary] = f_imageAnalysis(chBlue, chGreen, chRed, WellThis, FieldThis, PreviewPath, Layout)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% vol(chGreen, 0,15000)
   
    CellMaskMax = max(chGreen,[],3); %it(CellMaskMax);
    CellMask = CellMaskMax > 2000; %it(CellMask)
    CellMask = bwareaopen(CellMask, 500);
    
    
    CellRoxMean = mean(chRed,3);%it(CellRoxMean)
    RedVec = CellRoxMean(CellMask);
    
    Summary = table();
    Summary.Well = {WellThis};
    Summary.Field = {FieldThis};
    Summary.CellRoxMean = mean(RedVec);
    Summary = Iris_AnnotateTable(Summary, Layout, {'Barcode', 'CellLine', 'ExperimentalCondition'});

    %% Previews

    imSize = size(chBlue);
    [BarMask, BarCenter] = f_barMask(10, 0.10758027143330025, imSize, imSize(1)-50, 50, 20);
    %it(BarMask)

    RGB = cat(3, imadjust(max(chRed, [], 3), [0 0.02], [0 1]), imadjust(max(chGreen, [], 3), [0 1], [0 1]), imadjust(max(chBlue, [], 3), [0 0.05], [0 1]));
    RGB = f_imoverlayIris(RGB, BarMask, [1 1 1]);
     % imtool(RGB)
    R = f_imoverlayIris(imadjust(max(chRed, [], 3), [0 0.02], [0 1]), bwperim(CellMask), [0 1 0]);
    R = f_imoverlayIris(R, BarMask, [1 1 1]);
    % imtool(R) 
    
    G = f_imoverlayIris(imadjust(max(chGreen, [], 3), [0 1], [0 1]), bwperim(CellMask), [0 1 0]);
    G = f_imoverlayIris(G, BarMask, [1 1 1]);
    % imtool(G) 
     
    RGBPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_RGB.png'];
    imwrite(RGB, RGBPreviewPath)
    RPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_CellROX.png'];
    imwrite(R, RPreviewPath)
    GPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_CellTracker.png'];
    imwrite(G, GPreviewPath)

end


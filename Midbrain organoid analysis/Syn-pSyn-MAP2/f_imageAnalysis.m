function  [Objects] = f_imageAnalysis(Label, chHoechst, chMap2, chSynucleinP, chSynuclein, PreviewPath, MetaTable) %RemoveOuterRing
    
    %vol(chHoechst, 0, 10000)% Hoechst
    %vol(chMap2, 0, 5000)
    %vol(chSynucleinP, 0, 2000)
    %vol(chSynuclein, 0,5000)

%     %% Segment Organoid
%     chOrganoid = imlincomb(0.25, chHoechst, 0.25, chMap2, 0.25, chSynuclein, 0.25, chSynucleinP); %vol(chOrganoid, 0, 2000)
%     OrganoidMask = chOrganoid > 175;
%     %OrganoidMask = Iris_KeepbiggestObct3D(OrganoidMask);
% 
%     
%     OrganoidMask = bwareaopen(OrganoidMask, 1e2);
%     OrganoidMask = imclose(OrganoidMask, strel('disk', 50));
%     OrganoidMask = imclose(OrganoidMask, strel('sphere', 5));
%     OrganoidMask = logical(imfill(OrganoidMask, 'holes')); % vol(OrganoidMask,0,1)
%     
%     OrganoidMask = bwareaopen(OrganoidMask, 1e6);
%     
%     if RemoveOuterRing
%         OrganoidMaskEroded = imerode(OrganoidMask, strel('disk', 250));
%         OrganoidMaskOuterRingCandidates = OrganoidMask & ~OrganoidMaskEroded; % imtool(max(OrganoidMaskOuterRingCandidates,[],3))
%         OrganoidMaskOuterRingObjects = regionprops('table', bwlabeln(OrganoidMaskOuterRingCandidates), {'Area', 'PixelIdxList'});
%         OrganoidMaskOuterRingObjects = OrganoidMaskOuterRingObjects(OrganoidMaskOuterRingObjects.Area > 5e6, :); % todo  > 1e7
%         OrganoidMaskOuterRing = f_Create_Mask_from_ObjectList_Pixel_IDX(OrganoidMaskOuterRingObjects, 'PixelIdxList', OrganoidMask); % imtool(max(OrganoidMaskOuterRing,[],3))
%         OrganoidMaskCurated = OrganoidMask & ~OrganoidMaskOuterRing; % vol(OrganoidMaskCurated,0,1)
%     else
%         OrganoidMaskCurated = OrganoidMask;
%     end
% 
%     HairSmoothness = stdfilt(chSynuclein, ones(5)); %vol(HairSmoothness)
%     HairMask = chSynuclein > 1500; %vol(HairMask, 0,1)
%     HairObjectCandidates = regionprops('table', bwlabeln(HairMask), HairSmoothness, {'Area','MeanIntensity','PixelIdxList'});
%     HairObjectCandidates = HairObjectCandidates(HairObjectCandidates.Area > 1e4,:);
%     HairObjectCandidates = HairObjectCandidates(HairObjectCandidates.MeanIntensity < 130,:);
%     HairMask = f_Create_Mask_from_ObjectList_Pixel_IDX(HairObjectCandidates, 'PixelIdxList', HairMask); % vol(HairMask, 0,1)
%     OrganoidMaskCurated = OrganoidMaskCurated & ~HairMask;% vol(OrganoidMaskCurated,0,1)
  

if false % debug tool
        chHoechst = chHoechst(700:2700,3000:5000,:);
        chTH = chTH(700:2700,3000:5000,:);
        chTuj1 = chTuj1(700:2700,3000:5000,:);
        chMap2 = chMap2(700:2700,3000:5000,:);
    end
    
    %% Organoid segmentation
%     chCombo = imlincomb(0.25, chHoechst, 0.25, chMap2, 0.25, chSynucleinP, 0.25, chSynuclein);%vol(chCombo, 0, 500)
%     OrganoidMask = chCombo > 115;% vol(OrganoidMask)
%     OrganoidMask = bwareaopen(OrganoidMask, 1E6);
%     OrganoidMask = imclose(OrganoidMask, strel('sphere', 3));

    chOrganoid = imlincomb(0.25, chHoechst, 0.25, chMap2, 0.25, chSynucleinP, 0.25, chSynuclein);
    %chOrganoid = chHoechst; % other markers are condition dependent
    %vol(chOrganoid, 0, 1000)
    OrganoidMask = chOrganoid > 120; % vol(OrganoidMask,0,1)
    OrganoidMask = bwareaopen(OrganoidMask, 1e5);
    OrganoidMask = Iris_KeepbiggestObct3D(OrganoidMask);
    OrganoidMask = imclose(OrganoidMask, strel('sphere', 5));
    OrganoidMask = imfill(OrganoidMask, 'holes');
    
    
    %% Segment Dead core
    % strategy resize (shrink) DoG (expand interploate) 
    ImHoechstShrinked = imresize3(chHoechst, [size(chHoechst,1)/10, size(chHoechst,2)/10, size(chHoechst,3)]);% vol(ImHoechstShrinked, 0, 5000) % imtool(max(ch1,[],3))
    DeadCoreDoG = imfilter(ImHoechstShrinked, fspecial('gaussian', 1001, 1) - fspecial('gaussian', 1001, 201), 0); % vol(DeadCoreDoG,0,10000)sparse mosaic has zeros anyway
    DeadCoreMaskShrinked = DeadCoreDoG > 1200; % vol(DeadCoreMaskShrinked, 0,1)
    DeadCoreMaskShrinked = bwareaopen(DeadCoreMaskShrinked, 500);
    DeadCoreMaskShrinked = imclose(DeadCoreMaskShrinked, strel('disk', 7));
    DeadCoreMask = logical(imresize3(uint8(DeadCoreMaskShrinked), size(chHoechst))); % vol(DeadCoreMask, 0, 1)
    % vol(DeadCoreDoG, 0, 3000)
    
    OrganoidMaskWOCore = OrganoidMask & ~DeadCoreMask;
    
    %% Segment Nuclei
    % vol(chHoechst, 0, 1000)
    
    NucDoG = imfilter(double(chHoechst), fspecial('gaussian', 55, 1) - fspecial('gaussian', 55, 11), 'symmetric');%vol(NucDoG, 0, 100, 'hot')
    NucleiMask = NucDoG > 45; %vol(NucleiMask)
    NucleiMask = bwareaopen(NucleiMask, 100);%vol(NucleiMask)
    NucleiMask = NucleiMask | DeadCoreMask;
    
    ch1LP = imfilter(chHoechst, fspecial('gaussian', 11, 1), 'symmetric');
    NucMaskHigh =  (ch1LP > 2800) .* NucleiMask; %vol(NucMaskHigh, 0, 1)
    NucMaskAlive = NucleiMask & ~NucMaskHigh; % vol(NucMaskAlive)
    
    NucleiWODeadCore = NucleiMask & ~DeadCoreMask;
%      %% Segment Nuclei
%     NucMask = medfilt3(chHoechst) > 400;
%     NucMask = bwareaopen(NucMask, 100);
%     NucMask = NucMask & OrganoidMaskCurated; % vol(NucMask, 0,1)


    %% Segment Map2
   % vol(chMap2, 0, 1000) 
%     %% Map2 & Tuj1
%     % detect large structures
    for p=1:size(chMap2, 3)
        Map2_FT(:,:,p) = f_LPF_by_FFT(chMap2(:,:,p), 'Butterworth', [15,1], 0);
    end % vol(chMap2_FT * 1000, 0, 100, hot)

    Map2Mask = Map2_FT > 0.004; %0.008; % vol(Map2Mask)
    Map2Mask = bwareaopen(Map2Mask, 50); %27
    
    Map2MaskWODeadCore = Map2Mask & ~DeadCoreMask;
    
    clear('Map2_FT') 


%     %%%%%%%%%%%%%%%%%%%%%%%%%
%     Map2DoG = imfilter(chMap2, fspecial('gaussian', 105, 1) -  fspecial('gaussian', 105, 35), 'symmetric'); %vol(Map2DoG, 0, 500)
%     Map2DoGMask = Map2DoG > 100;
%     Map2Mask =  Map2DoGMask & OrganoidMaskCurated; % vol(Map2Mask,0,1)
%     %%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% Segment SynucleinP
    % Please exclude autofluoresc. and check if spots are synapses (i.e. flip 2ndary ABs)
    
    %SNCAMaskP = chSynucleinP > 2000; % vol(SNCAMask) %vol(chSynucleinP, 0, 3000)
    SNCAMaskP = chSynucleinP > 300; % vol(SNCAMaskP) %vol(chSynucleinP, 0, 3000)
    SNCAMaskP = bwareaopen(SNCAMaskP, 27); % vol(SNCAMaskP, 0, 1)
    %SNCAMaskP = SNCAMaskP & OrganoidMaskCurated;
    
    SNCAMaskPWODeadCore = SNCAMaskP & ~DeadCoreMask;
    
    %% Segment Synuclein
    % Please exclude autofluoresc. 
    
    SNCAMask = chSynuclein > 950; % vol(SNCAMask) %vol(chSynuclein, 0, 5000)
    SNCAMask = bwareaopen(SNCAMask, 27);
    %SNCAMask = SNCAMask & OrganoidMaskCurated;
    SNCAMaskWODeadCore = SNCAMask & ~DeadCoreMask;
    
%     % SNCA spots
%     SNCADoG = imfilter(chSynuclein, fspecial('gaussian', 55, 1)-fspecial('gaussian', 55, 5), 'symmetric');%vol(SNCADoG, 0, 3000)
%     SNCASpotMask = SNCADoG > 1000; % vol(SNCASpotMask, 0,1)
%     
%     NeuroCytoMask = Map2Mask & ~NucleiMask;
%     NeuroNucMask = Map2Mask & NucleiMask;
%     
%     %SNCASpotMask = SNCASpotMask & ~NucMask;
%     SNCASpotMaskNeuroNuc = SNCASpotMask & NeuroNucMask;
%     SNCASpotMaskNeuroNuc = bwareaopen(SNCASpotMaskNeuroNuc, 7);
%     
%     SNCASpotMaskNeuroCyto = SNCASpotMask & NeuroCytoMask;
%     SNCASpotMaskNeuroCyto = bwareaopen(SNCASpotMaskNeuroCyto, 7);
%     
%     %% SNCA spots
%     SNCAPDoG = imfilter(chSynucleinP, fspecial('gaussian', 55, 1)-fspecial('gaussian', 55, 5), 'symmetric');%vol(SNCAPDoG, 0, 1000)
%     SNCAPSpotMask = SNCAPDoG > 50; % vol(SNCAPSpotMask, 0,1)
%     
%     %SNCASpotMask = SNCASpotMask & ~NucMask;
%     SNCAP_SpotMaskNeuroNuc = SNCAPSpotMask & NeuroNucMask;
%     SNCAP_SpotMaskNeuroNuc = bwareaopen(SNCAP_SpotMaskNeuroNuc, 7); % vol(SNCAP_SpotMaskNeuroNuc, 0,1)
%     
%     SNCAP_SpotMaskNeuroCyto = SNCAPSpotMask & NeuroCytoMask;
%     SNCAP_SpotMaskNeuroCyto = bwareaopen(SNCAP_SpotMaskNeuroCyto, 7); % vol(SNCAP_SpotMaskNeuroCyto, 0,1)
    
    %%
    
    
    SNCAMaskNuc = SNCAMask & NucleiMask; % vol(SNCAMaskNuc, 0,1)
    %SNCAMaskExtraNuc = SNCAMask & ~NucMask; % vol(SNCAMaskExtraNuc, 0,1)
%     SNCAMaskExtraNuc = SNCAMask & NeuroCytoMask; % vol(SNCAMaskExtraNuc, 0,1)
    SNCAMaskPNuc = SNCAMaskP & NucleiMask; % vol(SNCAMaskPNuc, 0,1)
    %SNCAMaskPExtraNuc = SNCAMaskP & ~NucMask; % vol(SNCAMaskPExtraNuc, 0,1)
%     SNCAMaskPExtraNuc = SNCAMaskP & NeuroCytoMask; % vol(SNCAMaskPExtraNuc, 0,1)

    %% Feature extraction
    Objects = table();
    Objects.OrganoidIdx = MetaTable.Idx;
    Objects.Well = MetaTable.Well;
    Objects.AreaName = MetaTable.AreaName;
    Objects.OrganoidID = MetaTable.OrganoidID;
    
    % Volumes in voxels
    Objects.NucArea = sum(NucleiMask(:));
    %Objects.ExtraNucArea = sum(~NucMask(:));
%     Objects.ExtraNucArea = sum(NeuroCytoMask(:));
    
    Objects.Map2Area = sum(Map2Mask(:));
    Objects.SNCAArea = sum(SNCAMask(:));
    Objects.SNCAPArea = sum(SNCAMaskP(:));
    SNCAPAndSNCAMask = SNCAMask & SNCAMaskP;
    Objects.SNCAPAndSNCAArea = sum(SNCAPAndSNCAMask(:));
    SNCAPAndSNCAAndMap2Mask = SNCAMask & SNCAMaskP & Map2Mask;
    Objects.SNCAPAndSNCAAndMap2Area = sum(SNCAPAndSNCAAndMap2Mask(:));
    Objects.OrganoidVolume = sum(OrganoidMask(:));
    
    % added 20220701
    Objects.SNCAtotalInSNCAMask = sum(chSynuclein(SNCAMask));
    Objects.SNCAtotalInSNCAPMask = sum(chSynuclein(SNCAMaskP));
    Objects.SNCAPtotalInSNCAMask = sum(chSynucleinP(SNCAMask));
    Objects.SNCAPtotalInSNCAPMask = sum(chSynucleinP(SNCAMaskP));
    
    Objects.AreaSNCAMaskNuc = sum(SNCAMaskNuc(:));
%     Objects.AreaSNCAMaskExtraNuc = sum(SNCAMaskExtraNuc(:));
    Objects.AreaSNCAMaskPNuc = sum(SNCAMaskPNuc(:));
%     Objects.AreaSNCAMaskPExtraNuc = sum(SNCAMaskPExtraNuc(:));
    
    % SNCA
    Objects.TotalSNCAinNuc = sum(chSynuclein(SNCAMaskNuc));
    Objects.MeanSNCAinNuc = mean(chSynuclein(SNCAMaskNuc));
    Objects.TotalSNCAinNucNormByNucArea =  Objects.TotalSNCAinNuc / Objects.NucArea;
   % Objects.TotalSNCAExtraNuc = sum(chSynuclein(SNCAMaskExtraNuc));
    %Objects.MeanSNCAExtraNuc = mean(chSynuclein(SNCAMaskExtraNuc));
%     Objects.TotalSNCAExtraNucNormByExtraNucArea =  Objects.TotalSNCAExtraNuc / Objects.ExtraNucArea;
%     [~, Objects.SpotCountSNCA_Nuc] = bwlabeln(SNCAMaskNuc);
%     [~, Objects.SpotCountSNCA_Extranuc] = bwlabeln(SNCAMaskExtraNuc);
%     Objects.SpotTotalAreaSNCA_Nuc = sum(SNCAMaskNuc(:));
%     Objects.SpotTotalAreaSNCA_Extranuc =sum(SNCAMaskExtraNuc(:));
    
    % SNCA+P
    Objects.TotalSNCAMaskPinNuc = sum(chSynucleinP(SNCAMaskPNuc));
    Objects.MeanSNCAPinNuc = mean(chSynucleinP(SNCAMaskPNuc));
    Objects.TotalSNCAMaskPinNucNormByNucArea =  Objects.TotalSNCAMaskPinNuc / Objects.NucArea;
    %Objects.TotalSNCAPExtraNuc = sum(chSynucleinP(SNCAMaskPExtraNuc));
    %Objects.MeanSNCAPExtraNuc = mean(chSynucleinP(SNCAMaskPExtraNuc));
%     Objects.TotalSNCAPExtraNucByNucArea = Objects.TotalSNCAPExtraNuc / Objects.ExtraNucArea;
%     [~, Objects.SpotCountSNCAP_Nuc] = bwlabeln(SNCAP_SpotMaskNeuroNuc);
%     [~, Objects.SpotCountSNCAP_Extranuc] = bwlabeln(SNCAP_SpotMaskNeuroCyto);
%     Objects.SpotTotalAreaSNCAP_Nuc = sum(SNCAP_SpotMaskNeuroNuc(:));
%     Objects.SpotTotalAreaSNCAP_Extranuc =sum(SNCAP_SpotMaskNeuroCyto(:));
    
    % Normalized voxel/voxel
    %Objects.OrganoidVolumeCurated = sum(OrganoidMaskCurated(:));
    Objects.Map2VoxelsByNucVoxels = Objects.Map2Area / Objects.NucArea;
    Objects.SNCAVoxelsByNucVoxels = Objects.SNCAArea / Objects.NucArea;
    Objects.SNCAVoxelsByMap2Voxels = Objects.SNCAArea / Objects.Map2Area;
    Objects.SNCAPVoxelsByNucVoxels = Objects.SNCAPArea / Objects.NucArea;
    Objects.SNCAPVoxelsByMap2Voxels = Objects.SNCAPArea / Objects.Map2Area;
    Objects.SNCAPAndSNCAVoxelsByNucVoxels = Objects.SNCAPAndSNCAArea / Objects.NucArea;
    Objects.SNCAPAndSNCAAndMap2VoxelsByNucVoxels = Objects.SNCAPAndSNCAAndMap2Area / Objects.NucArea;
    
    
    % means in ROIs
    %Objects.MeanSNCAIntensityInOrganoidVoxels = mean(chSynuclein(OrganoidMask));
    Objects.MeanSNCAIntensityInNucVoxels = mean(chSynuclein(NucleiMask));
    Objects.MeanSNCAIntensityInMap2Voxels = mean(chSynuclein(Map2Mask));
    %Objects.MeanSNCAPIntensityInOrganoidVoxels = mean(chSynucleinP(OrganoidMask));
    Objects.MeanSNCAPIntensityInNucVoxels = mean(chSynucleinP(NucleiMask));
    Objects.MeanSNCAPIntensityInMap2Voxels = mean(chSynucleinP(Map2Mask));
    
    %CS
    Objects.OrganoidVol = sum(OrganoidMask(:));
    Objects.DeadCoreVol = sum(DeadCoreMask(:));
    Objects.OrganoidWOCoreVol = sum(OrganoidMaskWOCore(:));
    %Objects.DeadCoreVolNorm = Objects.DeadCoreVol / Objects.OrganoidVol;
    Objects.NucleiWODeadCoreVol = sum(NucleiWODeadCore(:));
    
    %Objects.Nuclei = sum(NucleiMask(:));
    Objects.NucleiDead = sum(NucMaskHigh(:));
    Objects.NucleiNorm = sum(NucleiMask(:)) / Objects.OrganoidVol;

  
    %% Previews 
    % Scalebar
    imSize = [size(chHoechst, 1), size(chHoechst, 2)];
    [BarMask, BarCenter] = f_barMask(200, 0.32393102760889064, imSize, imSize(1)-200, 200, 25);
    %it(BarMask)

    RGB = cat(3, imadjust(max(chSynucleinP, [], 3), [0 0.01], [0 1]), imadjust(max(chSynuclein, [], 3), [0 0.05], [0 1]), imadjust(max(chHoechst, [], 3), [0 0.03], [0 1]));
    RGB = imoverlay2(RGB, BarMask, [1 1 1]);
    %imtool(RGB)
    
    RGB2 = cat(3, imadjust(max(chSynucleinP, [], 3), [0 0.02], [0 1]), imadjust(max(chMap2, [], 3), [0 0.05], [0 1]), imadjust(max(chSynuclein, [], 3), [0 0.05], [0 1]));
    RGB2 = imoverlay2(RGB2, BarMask, [1 1 1]);
    %imtool(RGB2)
     
    %PreviewMap2 = imoverlay2(imadjust(max(chMap2,[],3), [0 0.5], [0 1]), bwperim(max(Map2Mask,[],3)), [1 0 0]);
    PreviewMap2 = f_imoverlayAlpha(imadjust(max(chMap2,[],3), [0 0.05], [0 1]), max(Map2Mask,[],3), [0 1 0], 0.1);
    PreviewMap2 = imoverlay2(PreviewMap2, BarMask, [1 1 1]);
    %imtool(PreviewMap2,[])
    
    
    PreviewNuclei = f_imoverlayAlpha(imadjust(max(chHoechst,[],3), [0 0.03], [0 1]), max(NucleiMask,[],3), [0 0 1], 0.03);
    PreviewNuclei = imoverlay2(PreviewNuclei, BarMask, [1 1 1]);
    %imtool(PreviewNuclei,[])
    
    PreviewNucDead = imoverlay2(imadjust(max(chHoechst,[],3),[0 0.03]), bwperim(max(NucMaskHigh,[],3)), [0 0 1]);
    PreviewNucDead = imoverlay2(PreviewNucDead, BarMask, [1 1 1]); %imtool(PreviewNucDead)

    PreviewSNCA = f_imoverlayAlpha(imadjust(max(chSynuclein,[],3), [0 0.05], [0 1]), max(SNCAMask,[],3), [1 1 0], 0.1);
    PreviewSNCA = imoverlay2(PreviewSNCA, BarMask, [1 1 1]);
    %imtool(PreviewSNCA,[])
    
%     PreviewSNCA_Spots = f_imoverlayAlpha(imadjust(max(chSynuclein,[],3), [0 0.1], [0 1]), max(SNCASpotMaskNeuroNuc,[],3), [1 1 0], 0.3);
%     PreviewSNCA_Spots = f_imoverlayAlpha(PreviewSNCA_Spots, max(SNCASpotMaskNeuroCyto,[],3), [1 0 1], 0.3);
%     PreviewSNCA_Spots = imoverlay2(PreviewSNCA_Spots, BarMask, [1 1 1]);
%     %imtool(PreviewSNCA_Spots,[])
    
    PreviewSNCAP = f_imoverlayAlpha(imadjust(max(chSynucleinP,[],3), [0 0.02], [0 1]), max(SNCAMaskP,[],3), [0 1 1], 0.1);
    PreviewSNCAP = imoverlay2(PreviewSNCAP, BarMask, [1 1 1]);
    %imtool(PreviewSNCAP,[])
    
%     PreviewSNCAP_Spots = f_imoverlayAlpha(imadjust(max(chSynucleinP,[],3), [0 0.02], [0 1]), max(SNCAP_SpotMaskNeuroNuc,[],3), [0 1 1], 0.3);
%     PreviewSNCAP_Spots =f_imoverlayAlpha(PreviewSNCAP_Spots, max(SNCAP_SpotMaskNeuroCyto,[],3), [1 0 1], 0.3);
%     PreviewSNCAP_Spots = imoverlay2(PreviewSNCAP_Spots, BarMask, [1 1 1]);
%     %imtool(PreviewSNCAP_Spots,[])

%     PreviewOrganoid = f_imoverlayAlpha(imadjust(max(chMap2,[],3), [0 0.1], [0 1]), max(OrganoidMask,[],3), [1 0 1], 0.05);
%     PreviewOrganoid = f_imoverlayAlpha(PreviewOrganoid, max(OrganoidMaskCurated,[],3), [0 1 0], 0.05);
%     PreviewOrganoid = imoverlay2(PreviewOrganoid, BarMask, [1 1 1]);
%     %imtool(PreviewOrganoid,[])

    %% Write Previews 
    IdentityString = ['PreviewOrganoid', '_', MetaTable.OrganoidID{:}];
    imwrite(RGB, [PreviewPath, filesep, IdentityString, '_RGB.png'])
    imwrite(RGB2, [PreviewPath, filesep, IdentityString, '_RGB2.png'])
    imwrite(PreviewMap2, [PreviewPath, filesep, IdentityString, '_Map2.png'])
    imwrite(PreviewNuclei, [PreviewPath, filesep, IdentityString, '_Nuc.png'])
    imwrite(PreviewSNCA, [PreviewPath, filesep, IdentityString, '_SNCA.png'])
    imwrite(PreviewSNCAP, [PreviewPath, filesep, IdentityString, '_SNCAP.png'])
%     imwrite(PreviewOrganoid, [PreviewPath, filesep, IdentityString, '_Organoid.png'])
%     imwrite(PreviewSNCA_Spots, [PreviewPath, filesep, IdentityString, '_SNCASpots.png'])
%     imwrite(PreviewSNCAP_Spots, [PreviewPath, filesep, IdentityString, '_SNCAPSpots.png'])
    imwrite(PreviewNucDead, [PreviewPath, filesep, IdentityString, 'PreviewNucDead.png'])

end

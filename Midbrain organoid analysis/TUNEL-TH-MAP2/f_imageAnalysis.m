function  [Objects] = f_imageAnalysis(Label, chHoechst, chMAP2, chTunel, chTH, PreviewPath, MetaTable)
    %vol(chHoechst, 0, 5000)% Hoechst imtool(max(chHoechst,[],3),[])
    %vol(chMAP2, 0, 2000)
    %vol(chTunel, 0, 5000)
    %vol(chTH, 0, 1000)

    disp('debug')
    if false % debug tool
        chHoechst = chHoechst(4500:6500,1600:3600,:);
        chMAP2 = chMAP2(4500:6500,1600:3600,:);
        chTunel = chTunel(4500:6500,1600:3600,:);
        chTH = chTH(4500:6500,1600:3600,:);
    end

    
    %% Segment Organoid
    chOrganoid = imlincomb(0.25, chHoechst, 0.25, chMAP2, 0.25, chTunel, 0.25, chTH);
    %vol(chOrganoid, 0, 1000)
    OrganoidMask = medfilt3(chOrganoid, [5,5,3]) > 300; % vol(OrganoidMask,0,1)
    OrganoidMask = imclose(OrganoidMask, strel('disk',15));
    OrganoidMask = Iris_KeepbiggestObct3D(OrganoidMask);
    OrganoidMask = imclose(OrganoidMask, strel('cuboid', [11,11,5]));
    OrganoidMask = imfill(OrganoidMask, 'holes');
    
    %% Segment Nuclei via Hoechst
    NucMask = chHoechst > 500; %vol(NucMask)
    NucMask = bwareaopen(NucMask, 250);
   
    %% Segment MAP2
    %MAP2Mask = chMAP2 > 1200; %vol(MAP2Mask, 0,1)
    %MAP2Mask = bwareaopen(MAP2Mask, 250);
    
    for p=1:size(chMAP2, 3)
        Map2_FT(:,:,p) = f_LPF_by_FFT(chMAP2(:,:,p), 'Butterworth', [15,1], 0);
    end % vol(Map2_FT * 1000, 0, 100, hot)

    MAP2Mask = Map2_FT > 0.003; %vol(MAP2Mask)
    MAP2Mask = bwareaopen(MAP2Mask, 27);
    
    clear('Map2_FT') 

    %% Segment Tunel
    % imtool(max(chTunel,[],3))
    %TunelMask = chTunel > 5000; %vol(TunelMask, 0,1)
    %%TunelMask = chTunel > 3000; %vol(TunelMask, 0,1)
    TunelDoG = imfilter(chTunel, fspecial('gaussian', 101, 1) - fspecial('gaussian', 101, 21), 'symmetric');
    %imtool(max(TunelDoG, [], 3))
    TunelMask = TunelDoG > 500; %vol(TunelMask, 0,1)
    TunelMask = bwareaopen(TunelMask, 100);


    %% Segment TH
    for p=1:size(chTH, 3)
        chTH_FT(:,:,p) = f_LPF_by_FFT(chTH(:,:,p), 'Butterworth', [15,1], 0);
    end % vol(chTH_FT * 10000, 0, 100, hot)
    %THMask = chTH_FT > 0.0035; %vol(THMask, 0,1)% 0.0020?
    THMask = chTH_FT > 0.0025; %vol(THMask, 0,1)% 0.0020?
    THMask = bwareaopen(THMask, 10);
    clear('chTH_FT') 
    
    %% Tunel & Hoechst
    TunelPosHoechstNeg = TunelMask & ~NucMask;
    %vol(TunelPosHoechstNeg + TunelMask, 0,2)
    
    %% Tunel & TH
    TunelTHposMask = imdilate(TunelMask, strel('disk', 7)) & THMask;
    TunelTHposMask = bwareaopen(TunelTHposMask, 27);
    TunelTHposMask = imreconstruct(TunelTHposMask, TunelMask);
    % vol(TunelTHposMask + TunelMask, 0,2)
    
    %% Tunel & MAP2
    TunelMAP2posMask = TunelMask & MAP2Mask;
    % vol(TunelMAP2posMask + TunelMask, 0,2)
    
    %% Tunel & MAP2 & TH
    TunelTHMAP2posMask = TunelTHposMask & MAP2Mask;
    % vol(TunelTHMAP2posMask + TunelMask, 0,2)
    
     %% TH skeleton3D EPFL
    disp('Start skel')
    tic
    skelTH = Skeleton3D(THMask);
    toc
    disp('Skel done')
    %vol(skelTH, 0, 1)
    [AdjacencyMatrixTH, nodeTH, linkTH] = Skel2Graph3D(skelTH,0);                       
    %imtool(AdjacencyMatrixTH, [])
    NodeTH = zeros(size(THMask), 'uint8');
    NodeIdxs = vertcat(nodeTH(:).idx);
    NodeTH(NodeIdxs) = 1;
    %vol(NodeTH)    
    if size(NodeIdxs, 1) == 0
        return
    end
    NodeTHPreview = uint8(skelTH) + NodeTH + uint8(THMask); 
    NodeTHPreview2D = max(NodeTHPreview, [], 3);
    %it(NodeTHPreview2D)
    %vol(NodeTHPreview, 0, 3, 'jet')    
    NodeDegreeVectorTH = sum(AdjacencyMatrixTH, 1);

    ZeroNodeExplanationNeeded = 0;
    if ZeroNodeExplanationNeeded
        ZeroNodes = find(NodeDegreeVectorTH == 0);
        ZeroNodesLinIdx = vertcat(nodeTH(ZeroNodes).idx);
        ZeroNodeMask = zeros(size(THMaskClipped), 'uint8');
        ZeroNodeMask(ZeroNodesLinIdx) = 1; %vol(ZeroNodeMask)
        NodePreviewZeroCase = uint8(skelTH) + NodeMaskTH + 10*uint8(ZeroNodeMask) + uint8(THMask);
    end  

    %% TH Fragmentation

    % Define structuring element for surface detection
    Conn6 = strel('sphere', 1); % 6 connectivity
    % Detect surface
    SurfaceTH = THMask & ~(imerode(THMask, Conn6));
    %vol(SurfaceTH)
    
    %% Feature extraction
    Objects = table();
    Objects.OrganoidIdx = MetaTable.Idx;
    Objects.Well = MetaTable.Well;
    Objects.AreaName = MetaTable.AreaName;
    Objects.OrganoidID = MetaTable.OrganoidID;
    Objects.NucArea = sum(NucMask(:));
    Objects.THArea = sum(THMask(:));
    Objects.MAP2Area = sum(MAP2Mask(:));
    Objects.TunelArea = sum(TunelMask(:));
    Objects.TunelPosHoechstNegArea = sum(TunelPosHoechstNeg(:));
    Objects.TunelPosHoechstNegAreaByTunelArea = Objects.TunelPosHoechstNegArea / Objects.TunelArea;
    Objects.TunelTHposArea = sum(TunelTHposMask(:));
    Objects.TunelTHposAreaByTunelArea = Objects.TunelTHposArea / Objects.TunelArea;
    Objects.TunelTHposAreaByHoechstArea = Objects.TunelTHposArea / Objects.NucArea;
    Objects.TunelTHposAreaByTHArea = Objects.TunelTHposArea / Objects.THArea;
    
    Objects.TunelMAP2posArea = sum(TunelMAP2posMask(:));
    Objects.TunelMAP2posAreaByTunelArea = Objects.TunelMAP2posArea / Objects.TunelArea;
    Objects.TunelMAP2posAreaByHoechstArea = Objects.TunelMAP2posArea / Objects.NucArea;
    Objects.TunelMAP2posAreaByMAP2Area = Objects.TunelMAP2posArea / Objects.MAP2Area;
    
    Objects.THAreaNorm2NucArea = Objects.THArea / Objects.NucArea;
    Objects.MAP2AreaNorm2NucArea = Objects.MAP2Area / Objects.NucArea;
    Objects.TunelAreaNorm2NucArea = Objects.TunelArea / Objects.NucArea;
    Objects.meanTHinTHMask = mean(chTH(THMask));
    Objects.OrganoidVolume = sum(OrganoidMask(:));
    
    Objects.SkelTH = sum(skelTH(:));
    Objects.Nodes = size(nodeTH, 2);
    Objects.Links = size(linkTH, 2);
    Objects.NodesNorm = Objects.Nodes / Objects.SkelTH;
    Objects.LinksNorm = Objects.Links / Objects.SkelTH;
    Objects.THFragmentation = sum(SurfaceTH(:)) / sum(THMask(:));

    %% Previews 
    % Scalebar
    imSize = [size(chHoechst, 1), size(chHoechst, 2)];
    [BarMask, BarCenter] = f_barMask(200, 0.32393102760889064, imSize, imSize(1)-200, 200, 25);
    %it(BarMask)

    
    RGB = cat(3, imadjust(max(chMAP2, [], 3), [0 0.1], [0 1]), imadjust(max(chTH, [], 3), [0 0.02], [0 1]), imadjust(max(chHoechst, [], 3), [0 0.07], [0 1]));
    RGB = imoverlay2(RGB, BarMask, [1 1 1]);
    %imtool(RGB)
    
    PreviewNuc = f_imoverlayAlpha(imadjust(max(chHoechst,[],3), [0 0.1], [0 1]), max(NucMask,[],3), [0 0 1], 0.1);
    PreviewNuc = imoverlay2(PreviewNuc, BarMask, [1 1 1]);
    %imtool(PreviewNuc,[])
    
    PreviewNucMask = imoverlay2(imadjust(max(chHoechst,[],3), [0 0.1], [0 1]), bwperim(max(NucMask,[],3)), [0 0 1]);
    PreviewNucMask = imoverlay2(PreviewNucMask, BarMask, [1 1 1]);
    %imtool(PreviewNucMask,[])
    
    PreviewTH = f_imoverlayAlpha(imadjust(max(chTH,[],3), [0 0.02], [0 1]), max(THMask,[],3), [0 1 0], 0.1);
    PreviewTH = imoverlay2(PreviewTH, BarMask, [1 1 1]);
    %imtool(PreviewTH,[])
    
    PreviewTHMask = imoverlay2(imadjust(max(chTH,[],3), [0 0.015], [0 1]), bwperim(max(THMask,[],3)), [0 1 0]);
    PreviewTHMask = imoverlay2(PreviewTHMask, BarMask, [1 1 1]);
    %imtool(PreviewTHMask,[])
    
        
    PreviewTunelMask = imoverlay2(imadjust(max(chTunel,[],3), [0 0.2], [0 1]), bwperim(max(TunelMask,[],3)), [1 0 0]);
    PreviewTunelMask = imoverlay2(PreviewTunelMask, BarMask, [1 1 1]);
    %imtool(PreviewTunelMask,[])
    
    PreviewTunel = f_imoverlayAlpha(imadjust(max(chTunel,[],3), [0 0.2], [0 1]), max(TunelMask,[],3), [1 1 0], 0.1);
    PreviewTunel= imoverlay2(PreviewTunel, BarMask, [1 1 1]);
    %imtool(PreviewTunel,[])
        
    PreviewMAP2Mask = imoverlay2(imadjust(max(chMAP2,[],3), [0 0.1], [0 1]), bwperim(max(MAP2Mask,[],3)), [1 0 0]);
    PreviewMAP2Mask = imoverlay2(PreviewMAP2Mask, BarMask, [1 1 1]);
    %imtool(PreviewMAP2Mask,[])
    
    PreviewMAP2 = f_imoverlayAlpha(imadjust(max(chMAP2,[],3), [0 0.1], [0 1]), max(MAP2Mask,[],3), [1 0 0], 0.1);
    PreviewMAP2= imoverlay2(PreviewMAP2, BarMask, [1 1 1]);
    %imtool(PreviewMAP2,[])
    

    %% Write Previews 
    IdentityString = ['PreviewOrganoid', '_', Objects.OrganoidID{:}]
    imwrite(RGB, [PreviewPath, filesep, IdentityString, 'RGB.png'])
    imwrite(PreviewNuc, [PreviewPath, filesep, IdentityString, 'Nuc.png'])
    imwrite(PreviewNucMask, [PreviewPath, filesep, IdentityString, 'NucMask.png'])
    imwrite(PreviewTH, [PreviewPath, filesep, IdentityString, 'TH.png'])
    imwrite(PreviewTHMask, [PreviewPath, filesep, IdentityString, 'THMask.png'])
    imwrite(PreviewTunel, [PreviewPath, filesep, IdentityString, 'Tunel.png'])
    imwrite(PreviewTunelMask, [PreviewPath, filesep, IdentityString, 'TunelMask.png'])
    imwrite(PreviewMAP2, [PreviewPath, filesep, IdentityString, 'MAP2.png'])
    imwrite(PreviewMAP2Mask, [PreviewPath, filesep, IdentityString, 'MAP2MAsk.png'])

    
%     %% save reconstructed raw channels as tif
%     mkdir([PreviewPath(1:end-8), 'Mosaics'])
% 
%     ChannelLabels = {'Hoechst', 'Tuj1', 'Map2', 'TH'};
%     for c = 1:numel(ChannelLabels)
%         t = Tiff([PreviewPath(1:end-8), 'Mosaics', filesep, Objects.OrganoidID{:}, '__', ChannelLabels{c},'.tif'], 'w');  
%         tagstruct.ImageLength = size(eval(['ch', ChannelLabels{c}]),1); 
%         tagstruct.ImageWidth = size(eval(['ch', ChannelLabels{c}]),2);
%         tagstruct.Photometric = Tiff.Photometric.MinIsBlack;
%         tagstruct.BitsPerSample = 16;
%         tagstruct.SamplesPerPixel = size(eval(['ch', ChannelLabels{c}]),3);
%         tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky; 
%         tagstruct.Software = 'MATLAB'; 
%         setTag(t,tagstruct)
%         write(t, eval(['ch', ChannelLabels{c}]));
%         close(t);
%     end

    

end
